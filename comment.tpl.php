<?php
?>
<div class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ' '. $status ?> clear-block <?php print $zebra ?>">
  <?php if ($picture): ?>
    <div class="avatar avatar-48">
      <?php print $picture ?>
    </div>
  <?php endif; ?>

  <?php if ($comment->new): ?>
    <span class="new"><?php print $new ?></span>
  <?php endif; ?>

  <div class="comment-meta">
    <?php print $submitted ?>
  </div>

  <div class="content">
    <?php print $content ?>
    <?php if ($signature): ?>
      <div class="user-signature clear-block">
        <?php print $signature ?>
      </div>
    <?php endif; ?>
  </div>

  <div class="comment-links">
    <?php print $links ?>
  </div>
</div>