<?php

require_once('theme-settings.inc');

/**
 * Implementation of THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   array An array of saved settings for this theme.
 * @return
 *   array A form array.
 */
function phptemplate_settings($saved_settings) {
  // Get theme settings
  $settings = inf08_theme_get_settings($saved_settings);

  /**
   * Create theme settings form widgets using Forms API
   */

  // Inf08 fieldset
  $form['inf08_container'] = array(
    '#type' => 'fieldset',
    '#title' => t('Inf08 settings'),
    '#description' => t('Use these settings to change what and how information is displayed in your theme.'),
    '#collapsible' => TRUE,
    '#collapsed' => false,
  );

  // General settings fieldset
  $form['inf08_container']['general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Number of comments in Recent comments block
  $form['inf08_container']['general_settings']['recent_comments'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Number of comments in Recent Comments block'),
      '#default_value' => $settings["recent_comments"],
      '#description'   => t('Number of comments to show in Recent comments block. Default is 5.'),
  );
  // Enable jQuery PNGFix
  $form['inf08_container']['general_settings']['jquery_pngfix'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use IE 6 PNG transparency fix'),
    '#default_value' => $settings['jquery_pngfix'],
  );
  // Show / hide breadcrumb
  $form['inf08_container']['general_settings']['breadcrumb_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display breadcrumb'),
    '#default_value' => $settings['breadcrumb_display'],
  );
  // Toggle WordPress-style "Read more" link
  $form['inf08_container']['general_settings']['wp_readmore'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use WordPress-style "Read more" link'),
    '#default_value' => $settings['wp_readmore'],
    '#description'   => t('The "Read more" link will be displayed right after the teaser which helps improve readability for the user.'),
  );
  // The weight of "Read more" link
  $form['inf08_container']['general_settings']['wp_readmore_weight'] = array(
      '#type'          => 'textfield',
      '#title'         => t('"Read more" weight'),
      '#default_value' => $settings["wp_readmore_weight"],
      '#description'   => t('You can set the weight of the WordPress-style "Read more" link in case it conflicts with other modules and show up in the wrong order.'),
  );

  // Mission statement settings fieldset
  $form['inf08_container']['mission_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mission statement settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Display mission statement on every pages?
  $form['inf08_container']['mission_settings']['mission_display'] = array(
    '#type' => 'radios',
    '#title' => t('Where should your mission statement be displayed?'),
    '#default_value' => $settings['mission_display'],
    '#options' => array(
      'front' => t('Display mission statement only on front page'),
      'all' => t('Display mission statement on all pages'),
    ),
  );
  // Set the title of mission statement
  $form['inf08_container']['mission_settings']['mission_title'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Mission statement title'),
      '#default_value' => $settings["mission_title"],
      '#description'   => t('The title of your mission statement. Leave blank to disable.'),
  );
  
  // Comment links settings fieldset
  $form['inf08_container']['comment_links_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Comment links settings'),
    '#description' => t('Here you can make adjustments to where the comment links should be displayed. You can modify these settings so they apply to all content types, or check the "Use content-type specific settings" box to customize them for each content type. For example, you may want to display comment links on top for stories, and on bottom for pages.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  foreach ((array('default' => 'Default') + node_get_types('names')) as $type => $name) {
    // Node type fieldset    
    $form['inf08_container']['comment_links_settings']['comment_links'][$type] = array(
      '#type' => 'fieldset',
      '#title' => t('!name', array('!name' => $name)),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    // Assign the comment links position per node type
    $form['inf08_container']['comment_links_settings']['comment_links'][$type]["comment_links_display_{$type}"] = array(
      '#type' => 'radios',
      '#title' => t('Where should the comment links be displayed?'),
      '#default_value' => $settings["comment_links_display_{$type}"],
      '#options' => array(
        'top' => t('At the top, along with post information'),
        'bottom' => t('At the bottom, along with node links'),
      ),
    );

    // Options for default settings
    if ($type == 'default') {
      $form['inf08_container']['comment_links_settings']['comment_links']['default']['#title'] = t('Default');
      $form['inf08_container']['comment_links_settings']['comment_links']['default']['#collapsed'] = $settings['comment_links_enable_content_type'] ? TRUE : FALSE;
      $form['inf08_container']['comment_links_settings']['comment_links']['comment_links_enable_content_type'] = array(
        '#type'          => 'checkbox',
        '#title'         => t('Use custom settings for each content type instead of the default above'),
        '#default_value' => $settings['comment_links_enable_content_type'],
      );
    }
    // Collapse content-type specific settings if default settings are being used
    else if ($settings['comment_links_enable_content_type'] == 0) {
      $form['comment_links_settings'][$type]['#collapsed'] = TRUE;
    }
  }

  // Category settings
  if (module_exists('taxonomy')) {
    // Category settings fieldset
    $form['inf08_container']['category_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Category settings'),
      '#description' => t('Here you can make adjustments to which vocabulary to be displayed as category. You can modify these settings so they apply to all content types, or check the "Use content-type specific settings" box to customize them for each content type. For example, you may want to display category on stories, but not pages.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    foreach ((array('default' => 'Default') + node_get_types('names')) as $type => $name) {
      // Get taxonomy vocabularies by node type
      $vocabs = array();
      $vocabs_by_type = ($type == 'default') ? taxonomy_get_vocabularies() : taxonomy_get_vocabularies($type);
      foreach ($vocabs_by_type as $key => $value) {
        $vocabs[$value->vid] = $value->name;
      }

      // Node type fieldset
      if (!empty($vocabs)) {
        $form['inf08_container']['category_settings']['category'][$type] = array(
          '#type' => 'fieldset',
          '#title' => t('!name', array('!name' => $name)),
          '#collapsible' => TRUE,
          '#collapsed' => ($settings['category_enable_content_type'] == 0) ? TRUE : FALSE,
        );

        // Assign vocabulary to be used as Category per node type
        $form['inf08_container']['category_settings']['category'][$type]["category_{$type}"] = array(
          '#type' => 'select',
          '#title' => t('Which vocabulary should be displayed as category?'),
          '#default_value' => $settings["category_{$type}"],
          '#options' => array(0 => '- none selected -') + $vocabs,
        );
      }

      // Options for default settings
      if ($type == 'default') {
        $form['inf08_container']['category_settings']['category']['default']['#title'] = t('Default');
        $form['inf08_container']['category_settings']['category']['default']['#collapsed'] = $settings['category_enable_content_type'] ? TRUE : FALSE;
        $form['inf08_container']['category_settings']['category']['category_enable_content_type'] = array(
          '#type'          => 'checkbox',
          '#title'         => t('Use custom settings for each content type instead of the default above'),
          '#default_value' => $settings['category_enable_content_type'],
        );
      }
      // Collapse content-type specific settings if default settings are being used
      else if ($settings['category_enable_content_type'] == 0) {
        $form['category_settings'][$type]['#collapsed'] = TRUE;
      }
    }
  }

  return $form;
}