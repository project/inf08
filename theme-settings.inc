<?php

/**
 * Returns default theme settings
 *
 * @return
 *    array Default theme settings array
 */
function inf08_theme_get_default_settings() {
  $defaults = array(
    'breadcrumb_display' => 1,
    'mission_display' => 'all',
    'mission_title' => '',
    'recent_comments' => 5,
    'category_enable_content_type' => 0,
    'category_default' => 0,
    'wp_readmore' => 1,
    'wp_readmore_weight' => 20,
    'comment_links_enable_content_type' => 0,
    'comment_links_display_default' => 'top',
    'jquery_pngfix' => 0,
  );

  return array_merge($defaults, theme_get_settings());
}

/**
 * Returns current theme settings
 *
 * @return
 *    array Current theme settings array
 */
function inf08_theme_get_settings($saved_settings) {
  $defaults = inf08_theme_get_default_settings();

  // Set the default values for content-type-specific settings
  foreach (node_get_types('names') as $type => $name) {
    $defaults["category_{$type}"] = $defaults['category_default'];
    $defaults["comment_links_display_{$type}"] = $defaults['comment_links_display_default'];
  }

  $settings = array_merge($defaults, $saved_settings);
  inf08_reset_content_type($settings);

  return $settings;
}

/**
 * Initialize theme settings
 *
 * Used when the user install the theme for the fist time
 * or when there is a new option in the theme settings
 */
function inf08_theme_init_settings() {
  global $theme_key;

  $defaults = inf08_theme_get_default_settings();
  $settings = theme_get_settings($theme_key);
  inf08_reset_content_type($settings);

  // Don't save the toggle_node_info_ variables
  if (module_exists('node')) {
    foreach (node_get_types() as $type => $name) {
      unset($settings['toggle_node_info_'. $type]);
    }
  }

  // Save default theme settings
  variable_set(
    str_replace('/', '_', 'theme_' . $theme_key . '_settings'),
    array_merge($defaults, $settings)
  );

  // Force refresh of Drupal internals
  theme_get_setting('', TRUE);
}

function inf08_reset_content_type(&$settings) {
  $node_types = node_get_types('names');

  // If content type-specifc settings are not enabled, reset the values
  if ($settings['category_enable_content_type'] == 0) {
    foreach ($node_types as $type => $name) {
      $settings["category_{$type}"] = $settings['category_default'];
    }
  }
  
  if ($settings['comment_links_enable_content_type'] == 0) {
    foreach ($node_types as $type => $name) {
      $settings["comment_links_display_{$type}"] = $settings['comment_links_display_default'];
    }
  }
}