<?php
?>
<p>
  <label for="qsearch"><?php print t('Search') ?>:</label>
  <input class="tbox" id="qsearch" type="text" name="search_theme_form" value="" title="<?php print t('Enter the terms you wish to search for.') ?>" />
  <?php print $search['hidden'] ?>
  <input class="btn" alt="Search" type="image" name="op" value="Search" title="Search" src="<?php print '/' . $directory ?>/images/search.gif" />
</p>