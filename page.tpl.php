<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <!--[if IE 6]>
  <link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . drupal_get_path('theme', 'inf08') ?>/css/ie.css" />
  <?php if (theme_get_setting('jquery_pngfix') == 1): ?>
  <script type="text/javascript" src="<?php print $base_path . drupal_get_path('theme', 'inf08') . '/js/jquery.pngFix.js' ?>"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $(document).pngFix();
    });
  </script>
  <?php endif; ?>
  <![endif]-->
</head>
<body>
  <!-- #container -->
  <div id="container">
    <div id="header-wrapper">
    <!-- #header -->
    <div id="header" class="<?php print $header_classes ?>">
    <?php if ($logo): ?>
        <a href="<?php print check_url($front_page) ?>" title="<?php print t('Go to Homepage') ?>"><?php print '<img src="'. check_url($logo) .'" alt="'. $site_name .'" id="logo" />' ?></a>
    <?php endif; ?>
    <?php if ($site_name): ?>
      <div id="site-name-slogan">
        <h1 id="site-name"><a href="<?php print check_url($front_page) ?>" title="<?php print t('Go to homepage') ?>"><?php print $site_name ?></a></h1>
        <?php if ($site_slogan): ?>
          <p><?php print $site_slogan ?></p>
        <?php endif; ?>
      </div>
    <?php endif; ?>
    <?php if ($search_box): ?>
      <div id="search-box"><?php print $search_box ?></div>
    <?php endif; ?>
    </div>
    <!-- end #header -->
    <!-- primary nav -->
    <?php if ($primary_links) : ?>
    <div id="primary-nav">
      <?php print $primary_links_tree; ?>
    </div>
    <?php endif; ?>
    </div>
    <!-- end #primary-nav -->
    <!-- #centent -->
    <div id="content">
      <?php if ($header): ?>
        <div id="content-header"><?php print $header ?></div>
      <?php endif; ?>
      <!-- #main -->
      <div id="main" class="<?php print $main_classes ?>">
        <?php print $breadcrumb ?>
        <?php if ($title): print '<h2>'. $title .'</h2>'; endif; ?>
        <?php if ($tabs) : ?>
          <ul class="tabs"><?php print $tabs ?></ul>
        <?php endif; ?>
        <?php if ($show_messages && $messages) : ?>
          <?php print $messages ?>
        <?php endif; ?>
        <?php if ($help) : ?>
          <?php print $help ?>
        <?php endif; ?>
        <?php print $content ?>
      </div>
      <!-- end #main -->

      <!-- #sidebar -->
      <div id="sidebar">
        <?php print $sidebar_top ?>
        <?php if ($mission) : ?>
        <div id="mission">
          <?php if ($mission_title) : ?>
            <h2><?php print $mission_title ?></h2>
          <?php endif ?>
          <p><?php print $mission ?></p>
        </div>
        <?php endif; ?>

        <!-- top sidebar -->
        <?php if ($sidebar): ?>
          <?php print $sidebar ?>
        <?php endif; ?>
        <!-- end top sidebar -->

        <!-- mini-sidebar (left) -->
        <?php if ($sidebar_left): ?>
        <div id="sidebar-left">
          <?php print $sidebar_left ?>
        </div>
        <?php endif; ?>
        <!-- end mini-sidebar (left) -->

        <!-- mini-sidebar (right) -->
        <?php if ($sidebar_right): ?>
        <div id="sidebar-right">
          <?php print $sidebar_right ?>
        </div>
        <?php endif; ?>
        <!-- end mini-sidebar (right) -->

      </div>
      <!-- end sidebar -->

    </div>
    <!-- end content -->

    <!-- footer -->
    <div id="footer">
    <?php if ($footer_message): ?>
      <p><?php print $footer_message ?></p>
    <?php endif; ?>

    <?php if ($footer) : ?>
      <?php print $footer ?>
    <?php endif; ?>
    </div>
    <!-- end footer -->

  </div>
  <!-- end container -->

  <?php print $closure ?>
</body>
</html>
