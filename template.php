<?php

require_once('theme-settings.inc');

/**
 * Initialize theme settings
 */
if (is_null(theme_get_setting('jquery_pngfix'))) {
  inf08_theme_init_settings();
}

function phptemplate_preprocess_page(&$vars) {
  $vars['header_classes'] = $vars['logo'] ? 'with-logo' : 'no-logo';
  
  if (theme_get_setting('breadcrumb_display') == 0) {
    $vars['breadcrumb'] = '';
  }

  if (theme_get_setting('toggle_mission') && theme_get_setting('mission_display') == 'all') {
    $vars['mission'] = variable_get('site_mission', '');
  }

  $vars['mission_title'] = theme_get_setting('mission_title');
  $vars['site_slogan'] = empty($vars['site_slogan']) ? '&nbsp;' : $vars['site_slogan'];

  $vars['main_classes'] = 'with-sidebar';

  // if no sidebar content, expand the main div to full width
  if (empty($vars['sidebar']) && empty($vars['sidebar_left']) && empty($vars['sidebar_right'])) {
    if (empty($vars['mission']) || (!drupal_is_front_page() && (theme_get_setting('mission_display') == 'front'))) {
      $vars['main_classes'] = 'no-sidebar';
    }
  }
  
  // generate menu tree from source of primary links
  $vars['primary_links_tree'] = menu_tree(variable_get('menu_primary_links_source', 'primary-links'));

  // prepare variables for footer credits
  $credits['drupal'] = l('Drupal', 'http://drupal.org', array('attributes' => array('title' => t('Official website of Drupal'))));
  $credits['inf08'] = l('Inf08', 'http://drupal.org/project/inf08', array('attributes' => array('title' => t('Inf08 theme project page'))));
  $credits['original-author'] = l('Inf Design', 'http://www.infscripts.com', array('attributes' => array('title' => t('Free CSS templates'))));
  $credits['author'] = l('kong', 'http://suksit.com', array('attributes' => array('title' => 'Suksit Sripitchayaphan')));

  $vars['footer'] .= t('!inf08 CSS template by !original-author. Ported to Drupal by !author', array('!inf08' => $credits['inf08'], '!original-author' => $credits['original-author'], '!author' => $credits['author']));
}

function phptemplate_preprocess_node(&$vars) {
  // add node regions content
  $vars['content_top'] = theme('blocks', 'content_top');
  $vars['content_bottom'] = theme('blocks', 'content_bottom');

  // format $submitted
  if (isset($vars['submitted']) && $vars['submitted'] != '') {
    $author = theme('username', $vars['node']);
    $vars['submitted'] = t('@date / Posted by !author', array('@date' => format_date($vars['created'], 'small'), '!author' => $author));
  }
  
  // node meta data
  $vars['meta'] = ((isset($vars['submitted']) && $vars['submitted'] != '') || isset($vars['comment_links'])) ? TRUE : FALSE;

  // format node links
  if (!empty($vars['node']->links)) {
    // add "Read the rest of this entry" link to the end of teaser like in WordPress
    if (theme_get_setting('wp_readmore') && isset($vars['node']->links['node_read_more'])) {
      $read_more_link = l(t('Read the rest of this entry') . ' »',
        'node/'. $vars['node']->nid,
        array(
          'attributes' => array(
            'title' => t('Read the rest of @node-title', array('@node-title' => $vars['node']->title)),
          ),
          'query' => NULL,
          'fragment' => NULL,
          'absolute' => FALSE,
          'html' => TRUE,
        )
      );

      $content = $vars['node']->content;

      $content['read-more'] = array(
        '#weight' => (int)theme_get_setting('wp_readmore_weight'),
        '#value' => '<p class="read-more">' . $read_more_link . '</p>',
        '#title' => '',
        '#description' => '',
      );

      // experimental code to handle CCK fields
      foreach ($content as $key => $value) {
        if (substr($key, 0, 6) == 'field_' && strpos($vars['content'], $vars[$key . '_rendered']) !== FALSE) {
          $content['cck_' . $key] = array(
            '#weight' => $content[$key]['#weight'],
            '#value' => $vars[$key . '_rendered'],
            '#title' => '',
            '#description' => '',
          );
        }
      }
      
      inf08_prepare_content($content);

      $vars['content'] = drupal_render($content);

      // remove the original "read more" link from $links
      unset($vars['node']->links['node_read_more']);
    }

    // put all comment related links into a separate variable
    foreach ($vars['node']->links as $key => $value) {
      if (substr($key, 0, 8) == 'comment_') {
        if ($key == 'comment_new_comments') {
          $tmp = explode(' ', $value['title']);
          $value['title'] = t('@count new', array('@count' => $tmp[0]));
          $comment_link = '(' . l($value['title'], $value['href'], array('attributes' => $value['attributes'], 'fragment' => $value['fragment'])) . ')';
        }
        elseif (empty($value['href'])) {
          $comment_link = $value['title'];
        }
        else {
          $comment_link = l($value['title'], $value['href'], array('attributes' => $value['attributes'], 'fragment' => $value['fragment']));
        }

        $vars['comment_links'] .= empty($vars['comment_links']) ? $comment_link : ' ' . $comment_link;

        unset ($vars['node']->links[$key]);
      }
    }
    
    $comment_links_content_type = (theme_get_setting('comment_links_enable_content_type') == 1) ? $vars['node']->type : 'default';
    $comment_links_display = theme_get_setting('comment_links_display_'. $comment_links_content_type);
    
    if ($comment_links_display == 'bottom' && !empty($vars['comment_links'])) {
      $vars['node']->links['comments'] = array(
        'title' => $vars['comment_links'],
        'html' => TRUE,
      );
      unset($vars['comment_links']);
    }
    
    // refresh $links
    $vars['links'] = theme('links', $vars['node']->links, array('class' => 'links inline'));
  }
  
  $vars['terms_classes'] = empty($vars['terms']) ? 'tags-empty' : 'tags';

  // format taxonomy list, code adapted from "Acquia Marina" theme
  if (module_exists('taxonomy')) {
    $vocabularies = taxonomy_get_vocabularies($vars['node']->type);
    $output = '';
    $vocab_delimiter = '';

    $category_content_type = (theme_get_setting('category_enable_content_type') == 1) ? $vars['node']->type : 'default';
    $category_setting = theme_get_setting('category_'. $category_content_type);

    foreach ($vocabularies as $vocabulary) {
      $terms = taxonomy_node_get_terms_by_vocabulary($vars['node'], $vocabulary->vid);

      if ($terms) {
        if ($vocabulary->vid != $category_setting) {
          $output .= '<li class="vocab vocab-'. $vocabulary->vid .'"><span class="vocab-name">'. check_plain($vocabulary->name) .':</span> <span class="vocab-list">';
          $links = array();

          foreach ($terms as $term) {
            $links[] = l($term->name, taxonomy_term_path($term), array('attributes' => array('rel' => 'tag', 'title' => strip_tags($term->description))));
          }

          $output .= implode(", ", $links);
          $output .= '</span></li>';
        }
        // Special case for vocabulary named "Category"
        else {
          $links = array();

          foreach ($terms as $term) {
            $links[] = l($term->name, taxonomy_term_path($term), array('attributes' => array('rel' => 'tag', 'title' => strip_tags($term->description))));
          }

          $category .= implode(", ", $links);
          $vars['category'] = t(' in ') . $category;
        }
      }
    }

    if ($output != '') {
      $output = '<ul class="taxonomy">'. $output .'</ul>';
    }

    $vars['terms'] = $output;
  }
  else {
    $vars['terms'] = '';
  }
}

function phptemplate_preprocess_comment_wrapper(&$vars) {
  // get total number of comments
  $comments_count = comment_num_all($vars['node']->nid);

  if ($comments_count == 0) {
    $vars['comments_count'] = t('No comments yet');
  }
  else {
    $vars['comments_count'] = t('@count @comment on "@node-title"', array('@count' => $comments_count, '@comment' => format_plural($comments_count, 'comment', 'comments'), '@node-title' => $vars['node']->title));
  }
}

function phptemplate_preprocess_comment(&$vars) {
  // change (not verified) to (visitor)
  $vars['author'] = str_replace(t('(not verified)'), t('(visitor)'), $vars['author']);

  // format $submitted
  $vars['submitted'] = t('!author said on !date:', array('!author' => $vars['author'], '!date' => $vars['date']));
}

/**
 * Implementation of hook_theme().
 */
function inf08_theme(){
  return array(
    'comment_form' => array(
      'arguments' => array('form' => NULL),
    ),
  );
}

/**
 * Theme the comment form
 */
function inf08_comment_form($form) {
  // Add a div wrapper for themeing.
  $form['#prefix'] = '<div id="writecomment">';
  $form['#suffix'] = '</div>';

  return drupal_render($form);
}

/**
 * Returns a formatted list of recent comments to be displayed in the comment block.
 *
 * Override the default number of 10 recent comments
 * and display the configured number of recent comments (default to 5).
 */
function phptemplate_comment_block() {
  $items = array();
  foreach (comment_get_recent(theme_get_setting('recent_comments')) as $comment) {
    $items[] = l($comment->subject, 'node/'. $comment->nid, array('fragment' => 'comment-'. $comment->cid)) .'<br />'. t('@time ago', array('@time' => format_interval(time() - $comment->timestamp)));
  }
  if ($items) {
    return theme('item_list', $items);
  }
}

/**
 * Recursively remove [#children] and [#printed] array entries
 * from content array to make it ready for drupal_render()
 */
function inf08_prepare_content(&$a) {
  foreach ($a as $key => $value) {
    if ($key == '#children' or $key == '#printed') {
      unset($a[$key]);
    }
    elseif (is_array($value)) {
      inf08_prepare_content($a[$key]);
    }
  }
}

function inf08_menu_item_link($link) {
  if (empty($link['localized_options'])) {
    $link['localized_options'] = array();
  }

  return l('<span>'.$link['title'].'</span>', $link['href'], $link['localized_options'] + array('html' => true));
}

