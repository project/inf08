<?php
?>
<div id="node-<?php print $node->nid; ?>" class="post node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">
  <?php if ($page == 0): ?>
    <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <?php endif; ?>

  <?php if ($meta): ?>
    <div class="meta post-info">
      <?php if ($submitted): ?>
        <div class="submitted">
          <?php print $submitted ?>
          <?php if ($category): ?><?php print $category ?><?php endif; ?>
        </div>
      <?php endif; ?>
      <?php if ($comment_links): ?>
        <div class="comments">
          <?php print $comment_links ?>
        </div>
      <?php endif; ?>
    </div>
  <?php endif; ?>

  <?php if ($content_top && !$teaser) : ?>
    <div id="content-top">
      <?php print $content_top ?>
    </div>
  <?php endif; ?>

  <div class="entry">
    <?php print $content ?>
  </div>

  <div class="<?php print $terms_classes ?>"><?php print $terms ?></div>

  <?php if ($links): ?>
    <div class="meta node-links">
      <?php print $links ?>
    </div>
  <?php endif; ?>

</div>
<?php if ($content_bottom && !$teaser) : ?>
  <div id="content-bottom">
    <?php print $content_bottom ?>
  </div>
<?php endif; ?>
